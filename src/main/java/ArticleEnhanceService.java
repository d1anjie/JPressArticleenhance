import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jpress.module.article.model.Article;
import io.jpress.module.article.service.ArticleService;

public interface ArticleEnhanceService extends ArticleService {

    public Page<Article> paginateByCategoryIdInNormal(int page, int pagesize, long categoryId,Integer tagid,String where,Object... para);

    public Page<Article> paginateByCategoryIdInNormal(int page, int pagesize, long categoryId,Integer tagid);

    public Page<Article> paginateFourElements(int page, int pagesize, long categoryId, long tagid, String fieldName,String value,String orderBy);

    public Page<Record> paginateBysql(int page, int pageSize, String sql);

    public Page<Article> paginate(Integer page,Integer pageSize, SqlPara sqlPara);
}
