import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.module.article.model.Article;
import io.jpress.web.base.TemplateControllerBase;

/**
 * 文章增强API接口
 */
@RequestMapping("/articleWebApi")
public class ArticleEnhanceAPIController extends TemplateControllerBase {

    @Inject
    private ArticleEnhanceService service;

    public void getJsonWhereYear(){

        Integer page = getParaToInt("page", 1);
        Integer pageSize = getParaToInt("pageSize", 10);
        Integer categoryId = getParaToInt("categoryId");
        Integer tagid = getParaToInt("tagid");
        Integer year  = getParaToInt("year");
        Page<Article> articlePage;

        //按照年查询
        if(categoryId!=null && tagid!=null && year!=null && year > 0){
            String where = " DATE_FORMAT(article.created,'%Y') = ?";
            articlePage = service.paginateByCategoryIdInNormal(
                            page, pageSize,
                            categoryId, tagid, where,year.toString());
        }else if(categoryId!=null && tagid==null && year!=null && year > 0){

            SqlPara sqlPara = new SqlPara();
            sqlPara.setSql(
                    "select * from article p,article_category_mapping acm where acm.category_id = ? and acm.article_id = p.id and p.status='normal' and DATE_FORMAT(p.created,'%Y') = ? order by created desc"
            );
            sqlPara.addPara(categoryId);
            sqlPara.addPara(year);
            articlePage = service.paginate(page, pageSize, sqlPara);

        }else if(categoryId!=null && tagid!=null){
            articlePage = service.paginateByCategoryIdInNormal(
                    page, pageSize,
                    categoryId, tagid);
        }else{
            SqlPara sqlPara = new SqlPara();
            sqlPara.setSql(
                    "select * from article p,article_category_mapping acm where acm.category_id = ? and acm.article_id = p.id and p.status='normal' order by created desc"
            );
            sqlPara.addPara(categoryId);
            articlePage = service.paginate(page, pageSize, sqlPara);
        }

        renderJson(articlePage);
    }


}
