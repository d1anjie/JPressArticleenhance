import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

import java.util.List;


@JFinalDirective("ProductCategoryById")
public class ProductCategoryByIdDirective extends JbootDirectiveBase {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        String id = getPara("categoryid", scope);
        Record records = Db.findFirst("select * from product_category where id = ?", id);
        scope.setGlobal("ProductCategoryById",records);
        renderBody(env, scope, writer);

    }

    @Override
    public boolean hasEnd() {
        return true;
    }

}
