import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.web.base.ControllerBase;

@RequestMapping("/productapi")
public class ProductAPIController extends ControllerBase {


    public void index(){
        Integer page = getParaToInt("page", 1);
        Integer pageSize = getParaToInt("pageSize", 10);
        Integer categoryid = getParaToInt("categoryid");
        SqlPara sqlPara = new SqlPara();
        sqlPara.setSql(
                "select * from product p,product_category_mapping pcm where pcm.category_id = ? and pcm.product_id = p.id "
        );
        sqlPara.addPara(categoryid);
        Page<Record> paginate = Db.paginate(page, pageSize, sqlPara);
        renderJson(paginate);
    }

}
