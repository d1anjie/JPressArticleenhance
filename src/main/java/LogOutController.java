import io.jboot.utils.CookieUtil;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.JPressConsts;
import io.jpress.web.base.TemplateControllerBase;

/**
 * 退出登录
 */
@RequestMapping("/logoutapi")
public class LogOutController extends TemplateControllerBase {

    public void logout() {
        CookieUtil.remove(this, JPressConsts.COOKIE_UID);
        redirect("/");
    }

}
