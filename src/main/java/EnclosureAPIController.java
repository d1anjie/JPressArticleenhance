import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import io.jboot.web.controller.annotation.RequestMapping;

import io.jpress.service.AttachmentService;
import io.jpress.web.base.TemplateControllerBase;

/**
 *
 */
@RequestMapping("/articleEnhanceApi")
public class EnclosureAPIController extends TemplateControllerBase {

    public void index(){

        String path = getPara("path");
        Record first = Db.findFirst("select title from attachment where path =?", path);
        renderJson(first);

    }

}
