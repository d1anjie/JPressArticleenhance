import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

import java.util.List;

/**
 * 按照标识查询商品分类
 * anjie 2020年4月17日
 */
@JFinalDirective("ProductByFlagList")
public class ProductByFlagListDirective extends JbootDirectiveBase {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        String flag = getPara("flag", scope);
        List<Record> records = Db.find("select * from product_category where flag = ?", flag);
        scope.setGlobal("ProductByFlagList",records);
        renderBody(env, scope, writer);

    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
