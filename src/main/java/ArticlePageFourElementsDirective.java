/**
 * Copyright (c) 2016-2020, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.controller.JbootControllerContext;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jboot.web.directive.base.PaginateDirectiveBase;
import io.jpress.JPressOptions;
import io.jpress.commons.directive.DirectveKit;
import io.jpress.module.article.model.Article;
import io.jpress.module.article.model.ArticleCategory;
import io.jpress.module.article.service.ArticleService;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Michael Yang 杨福海 （fuhai999@gmail.com）
 * @version V1.0
 */
@JFinalDirective("ArticlePageFourElementsPage")
public class ArticlePageFourElementsDirective extends JbootDirectiveBase {

    @Inject
    private ArticleEnhanceService service;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        Controller controller = JbootControllerContext.get();

        int page = controller.getParaToInt(1, 1);
        int pageSize = getParaToInt("pageSize", scope, 10);
        String orderBy = getPara("orderBy", scope, "id desc");
        Long categoryId = getPara("categoryId", scope);
        Integer tagid = getPara("tagid", scope);
        String field_Name = getPara("field_name", scope);
        Long c_categoryId = controller.getParaToLong("c_categoryId");
        Long c_tagid = controller.getParaToLong("c_tagid");


        String fieldName = controller.getPara("fieldName");
        String value = controller.getPara("value");
        String keyword = controller.get("keyword");

        Page<Article> articlePage;
        if(field_Name!=null && !field_Name.trim().equals("") && keyword!=null && !keyword.trim().equals("")) {
            String where = field_Name + " like ?";
            articlePage = service.paginateByCategoryIdInNormal(page, pageSize, categoryId, tagid,where,"%"+keyword+"%");
        }else if(c_categoryId!=null && c_tagid!=null && fieldName!=null && !fieldName.trim().equals("")&&
                value!=null && !value.trim().equals("")){
            articlePage = service.paginateFourElements(page, pageSize, c_categoryId, c_tagid, fieldName, value, orderBy);
        }else{
            articlePage = service.paginateByCategoryIdInNormal(page, pageSize, categoryId,tagid);
        }

        scope.setGlobal("ArticlePageFourElementsPage",articlePage);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }


    @JFinalDirective("ArticlePageFourElementsPaginate")
    public static class TemplatePaginateDirective extends PaginateDirectiveBase {

        @Override
        protected String getUrl(int pageNumber, Env env, Scope scope, Writer writer) {
            HttpServletRequest request = JbootControllerContext.get().getRequest();
            String url = request.getRequestURI();
            String contextPath = JFinal.me().getContextPath();

            boolean firstGotoIndex = getPara("firstGotoIndex", scope, false);

            if (pageNumber == 1 && firstGotoIndex) {
                return contextPath + "/";
            }

            // 如果当前页面是首页的话
            // 需要改变url的值，因为 上一页或下一页是通过当前的url解析出来的
            if (url.equals(contextPath + "/")) {
                url = contextPath + "/article/category/index"
                        + JPressOptions.getAppUrlSuffix();
            }
            return DirectveKit.replacePageNumber(url, pageNumber);
        }

        @Override
        protected Page<?> getPage(Env env, Scope scope, Writer writer) {
            return (Page<?>) scope.get("ArticlePageFourElementsPage");
        }

    }
}
