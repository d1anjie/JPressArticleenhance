import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import org.apache.commons.lang.StringEscapeUtils;

@JFinalDirective("EscapeHtml")
public class HTMLtoolsDirective extends JbootDirectiveBase {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String para = getPara(0, scope);
        String s = StringEscapeUtils.escapeHtml(para);
        scope.setGlobal("escape",s);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
