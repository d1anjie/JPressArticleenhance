
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

import java.util.List;

@JFinalDirective("ProductImageList")
public class ProductImageDirectve extends JbootDirectiveBase {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        Object id = getPara("id", scope);
        Integer pagesize = getParaToInt("pagesize", scope);
        Integer page = getParaToInt("page", scope);
        List<Record> records;
        if(pagesize!=null && page!=null){
            records = Db.find("select * from product_image where product_id = ?  limit ?,?", id,page*pagesize,pagesize);
        }else{
            records = Db.find("select * from product_image where product_id = ?", id);
        }

        scope.setGlobal("ProductImageList", records);
        renderBody(env, scope, writer);

    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
