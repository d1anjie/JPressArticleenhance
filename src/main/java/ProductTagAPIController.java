import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.web.base.ControllerBase;

@RequestMapping("/producttagapi")
public class ProductTagAPIController extends ControllerBase {


    public void index(){
        Integer page = getParaToInt("page", 1);
        Integer pageSize = getParaToInt("pageSize", 10);
        Integer categoryid = getParaToInt("categoryid");
        Integer tagid = getParaToInt("tagid");
        SqlPara sqlPara = new SqlPara();
        sqlPara.setSql(
                "select p.* from " +
                "(select a.product_id id from " +
                "(select * from product_category_mapping where category_id = ?) a," +
                "(select * from product_category_mapping where category_id = ?) b " +
                "where a.product_id = b.product_id) t,product p where t.id = p.id"
        );
        sqlPara.addPara(categoryid);
        sqlPara.addPara(tagid);
        Page<Record> paginate = Db.paginate(page, pageSize, sqlPara);
        renderJson(paginate);
    }

}
