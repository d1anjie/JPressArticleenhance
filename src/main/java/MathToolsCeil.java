import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

import java.util.List;

@JFinalDirective("MathToolsCeil")
public class MathToolsCeil extends JbootDirectiveBase {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        int n = getPara("n", scope);
        int i = getPara("i", scope);
        int ceil = (int) Math.ceil(Double.valueOf(n)/Double.valueOf(i));
        scope.setGlobal("ceil", ceil);
        renderBody(env, scope, writer);

    }

    @Override
    public boolean hasEnd() {
        return true;
    }

}
